﻿using ExcelDataReader;
using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Configuration;

namespace XL_Import
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string postURL = "http://smsReceive-production.h5s7dqspsv.us-east-1.elasticbeanstalk.com/twilio";
            StringBuilder sb = new StringBuilder();
            DataTable dt2 = new DataTable();
            dt2.Columns.Add(new DataColumn("FileName"));
            dt2.Columns.Add(new DataColumn("OriginalNumber"));
            dt2.Columns.Add(new DataColumn("CleanNumber"));
            dt2.Columns.Add(new DataColumn("CleanGUID"));
            dt2.Columns.Add(new DataColumn("Result"));
            dt2.Columns.Add(new DataColumn("isValid", typeof(bool)));


            BindingSource bs = new BindingSource();
            bs.DataSource = dt2;
            dataGridView1.DataSource = bs;

            using (var openFileDialog1 = new OpenFileDialog { Filter = "Excel Workbook|*.xls;*.xlsx;*.xlsm", ValidateNames = true })
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    importFile.Text = openFileDialog1.FileName;
                    using (var stream = File.Open(openFileDialog1.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();
                            DataTable dt = result.Tables[(result.Tables.Count - 1)];

                            foreach (DataRow r in dt.Rows)
                            {
                                var phoneNumber = ValidatePhone(Convert.ToString(r[20]));
                                //We expect phone number in column U(0 based = column 20)
                                // phone number tuple:
                                //Item1 = clean phone number
                                //  Item2 = isValid boolean
                                //  Item3 = clean GUID
                                string[] row = new string[] { openFileDialog1.FileName, Convert.ToString(r[20]), phoneNumber.Item1, phoneNumber.Item3, null, phoneNumber.Item2.ToString() };
                                dt2.Rows.Add(row);

                                if ((phoneNumber.Item2 == true) && (!ExistsTOI(phoneNumber.Item1)))
                                {
                                    sb.Clear();
                                    sb.Append("ApiVersion=2010-04-01&NumSegments=1&ToState=IA" +
                                        "&AccountSid=AC699f2693f3a4e733584ae3e83e365376" +
                                        "&FromCountry=US&To=82928&Body=Taponit&ToCountry=US&NumMedia=0");
                                    sb.Append("&SmsSid=SM" + phoneNumber.Item3);
                                    sb.Append("&SmsStatus=received");
                                    sb.Append("&SmsMessageSid=SM" + phoneNumber.Item3);
                                    sb.Append("&MessageSid=SM" + phoneNumber.Item3);
                                    sb.Append("&From=" + phoneNumber.Item1);

                                    string msgID = Guid.NewGuid().ToString();

                                    msgID = msgID.Replace("-", "");


                                    sb.Append("ApiVersion=2010-04-01&NumSegments=1&ToState=IA" +
                                                                "&AccountSid=AC699f2693f3a4e733584ae3e83e365376" +
                                                                "&FromCountry=US&To=82928&Body=02919&ToCountry=US&NumMedia=0");
                                    sb.Append("&SmsSid=SM" + msgID);
                                    sb.Append("&SmsStatus=received");
                                    sb.Append("&SmsMessageSid=SM" + msgID);
                                    sb.Append("&MessageSid=SM" + msgID);
                                    sb.Append("&From=+14012417348");

                                    try
                                    {
                                        var url = postURL.PostStringAsync(sb.ToString());
                                    }
                                    catch (FlurlHttpException ex)
                                    {
                                        //For error responses that take an unknown shape
                                        dynamic d = ex.GetResponseJsonAsync();
                                    }
                                }
                            }  // foreach DataRow

                            var connection = ConfigurationManager.ConnectionStrings["XLImportLogConnectionString"].ConnectionString;
                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                            {
                                bulkCopy.DestinationTableName = "dbo.ImportedNumbers";
                                SqlBulkCopyColumnMapping fn = new SqlBulkCopyColumnMapping("FileName", "FileName");
                                bulkCopy.ColumnMappings.Add(fn);

                                SqlBulkCopyColumnMapping orig = new SqlBulkCopyColumnMapping("OriginalNumber", "OriginalNumber");
                                bulkCopy.ColumnMappings.Add(orig);

                                SqlBulkCopyColumnMapping cn = new SqlBulkCopyColumnMapping("CleanNumber", "CleanNumber");
                                bulkCopy.ColumnMappings.Add(cn);

                                SqlBulkCopyColumnMapping cg = new SqlBulkCopyColumnMapping("CleanGUID", "CleanGUID");
                                bulkCopy.ColumnMappings.Add(cg);

                                SqlBulkCopyColumnMapping re = new SqlBulkCopyColumnMapping("Result", "Result");
                                bulkCopy.ColumnMappings.Add(re);

                                SqlBulkCopyColumnMapping iv = new SqlBulkCopyColumnMapping("isValid", "isValid");
                                bulkCopy.ColumnMappings.Add(iv);
                                try
                                {
                                    // Write from the source to the destination.
                                    bulkCopy.WriteToServer(dt2);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            } // using bulk copy
                        }
                    }
                }
            }

        }

        private bool ExistsTOI(string phoneIn)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["XLImportLogConnectionString"].ConnectionString);
            bool exists = false;
            try
            {
                string query = "SELECT COUNT(*) AS CNT FROM SUBSCRIBER WHERE NUMBER = '" + phoneIn.Replace("+", "") + "'";
                conn.Open();

                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Int16 cnt = Convert.ToInt16(dr[0].ToString());

                        if (cnt > 0)
                        {
                            exists = true;
                        }
                        else
                        {
                            exists = false;
                        }
                    }
                }

                dr.Close();
                cmd.Dispose();
                conn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! " + ex.Message);
                exists = false;
            }

            return exists;

        }

        private static Tuple<string, bool, string> ValidatePhone(string phoneIn)
        {
            Regex rgxPHONE = new Regex("\\D");
            Regex rgxGUID = new Regex("-*");
            Guid g;
            string gid = "";
            string phone = "";

            phone = rgxPHONE.Replace(phoneIn, "");
            bool isValid = false;
            switch (phone.Length)
            {
                case 10:
                    phone = "+1" + phone;
                    isValid = true;
                    break;
                case 11:
                    if (phone.IndexOf("1") == 0)
                    {
                        phone = "+" + phone;
                        isValid = true;
                    }
                    else
                    {
                        //bad number
                        //Console.Write("BAD: " + phone);
                        isValid = false;
                    }
                    break;
                default:
                    //Console.Write("BAD: " + phone);
                    isValid = false;
                    //bad number
                    break;
            }
            if (isValid)
            {
                // Create and display the value of two GUIDs.
                g = Guid.NewGuid();
                gid = rgxGUID.Replace((g.ToString()), "");
            }
            var tuple = new Tuple<string, bool, string>(phone, isValid, gid);
            return tuple;
        }

    }
}




